from RESTester import Tester

tester = Tester('http://localhost:8000/')

tester.create_test(
    'GET',
    'categories/',
    assertion={
        "error": {
            "message": "no error",
            "errorno": "0"
        },
        "success" : True,
        "data": [{
            "id":"[0-9]+",
            "description":"[a-zA-Z]+"
        }]
    })

tester.evaluate_queued_tests(debug=True)
tester.summary()
