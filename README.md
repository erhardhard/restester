[TOC]

# About this project #
This python module was created to create simple tests for REST based API-Endpoints.
It can be used in your own python scripts and as a commandlinetool.

At the moment, this tool is only tested with Python2.7.12.
When I finde some free time, I will try to port this tool to Python3.

**It is only possible to process json styled responses!1!**

# Setup #
Clone this repo and in the newly cloned directory execute `pip install -r .\RESTester\requirements.txt`.

# Usage #
## From Another Script ##
Import the Tester with `from RESTester import Tester`.
This Object handles all your testing and is initialized via `tester = Tester("http://url_to_your_server:8080/")`.
The specified Url is the root address of your webserver.
All following tests will be executed on this Url.

To create a test, you have to specify the assertion you make, about the response from your server.
An Assertion is a list or a dict which represents the data, wich is responded from your API.
Lets assume we get the following response from a GET call to `http://url_to_your_server:8080/foobar/`
```json
{
  "error": {
    "message": "no error",
    "errorno": "0"
  },
  "success": true,
  "data": [
    {
      "id": 1,
      "description": "Foo"
    },
    {
      "id": 2,
      "description": "Bar"
    }
  ]
}
```
Because python and json have a very similar style when declaring data structures we can just copy this text and use it as our assertion.
Let's imagine the `data` array of our json response has a length of over 500 entries.
GL & HF squeezing this monster in your python script.
To simplify this process and prevent unnecessary typos it is possible to replace fixed data with regular expressions.
This allows us to reduce the above example to
```json
{
  "error": {
    "message": "no error",
    "errorno": "0"
  },
  "success": true,
  "data": [
    {
      "id": "[0-9]+",
      "description": "[a-zA-Z0-9]+"
    }
  ]
}
```
Well, its less than expected but imagine the space savings with larger datasets.

With this assertion ready to go, you can create your first test.
```python
tester.create_test(
    'GET',
    'foobar/',
    assertion={
        "error": {
            "message": "no error",
            "errorno": "0"
        },
        "success" : True,
        "data": [{
            "id": "[0-9]+",
            "description": "[a-zA-Z]+"
        }]
    }
)
```
The first parameter of this function call defines the method of this request.
GET, POST, PATCH, PUT and DELETE are possible.
The second the route to execute this test on.
This string will be concatenated to the url you specified when creating the tester.
The third parameter specifies the assertion you except from the server.

You can queue as many test as you like with this function call.
When you are finished, you can call `tester.evaluate_queued_tests()`.
The tester will go trough each test, execute the planned request and test if your assertion matches the result.
After this process is finished, you can print all results with `tester.summary()`

You can find more information on all available functions and how to use them in the later chapters.

## From The Commandline ##
Always setting up a python script and running your tests from within this is good if you like to have the full control over all results.
If you just want to test your endpoints and get a nice summary at the end without to much python coding, you can use the RESTester directly from the commandline.
To do so, you need to specify all tests in one or more test-files.
These files are simple, text based files with a special syntax.
The first line needs to contain the version of the test-file-parser.
There is currently only one version so this is not very complicated, just add `#version 1` as the first line to your file and you are fine.
The next line contains the root address of you webserver, e.g. `http://url_to_your_server:8080/`
Your header should now look like this:
```
#version 1
http://url_to_your_server:8080/
```
Whit all the necessary lines in place, you can define your first test.
To do so, you create a test-block with
```
TEST MyFirstTestYay
    ...
END
```
After the keyword `TEST` you can specify the title of this test.
This comes in handy When printing the summary.
In this test-block, you can specify all nevessary data for your test:
```
#version 1
http://url_to_your_server:8080/

TEST MyFirstTestYay
    "method"    : "GET",
    "url"       : "foobar/",
    "assertion" : {
        "error": {
            "message": "no error",
            "errorno": "0"
        },
        "success" : True,
        "data": [{
            "id": "[0-9]+",
            "description": "[a-zA-Z]+"
        }]
    }
END
```
As you can see, this example is the same as the example in the last chapter.
You can define as many test as you like in a single file.
You only need to split them into blocks.

If you like to make a POST or PATCH request, you can specify them via:
```
#version 1
http://url_to_your_server:8080/

TEST MySecondTestWelp
    "method"    : "POST",
    "url"       : "foobar/",
    "assertion" : {
        "error": {
            "message": "no error",
            "errorno": "0"
        },
        "success" : True,
        "data": {}
    },
    "payload" : {
        "foo"   : "bar",
        "adams" : 42,
    },
    "files"   : {
        "avatar" : "./avatar.jpg"
    }
END
```

With such a test-file ready to go, you can start the commandlinetool with `python RESTester MyTests.rtr`.
All tests will be executed and a summary is printed to the stdout.
You can specify a output file with the argument `python RESTester MyTests.rtr -o summary.html`.
This will generate an easy to read html file with all relevant information in it and a visual mark for all failed tests.

# Developer Interface #
There are two classes: Tester and TestObject.

## TestObject ##
### TestObject Fields ###
Field | Type | Info
---| --| --| --
title | String  | 
method | String | Method of this test. GET, POST, ...
url | String | 
assertion | Dict or List | 
payload | Dict | 
files | Dict | 
result_raw | String | Raw string from the API
result | Dict or List | Converted response from the API
status | Int | HTTP-Statuscode of the request
passed | Boolean | False if the test failed
executed | Boolean | True if the test was executed
error_stack | List of Strings | List of all errors that occured during the testing

### TestObject Functions ###

#### TestObject.push_error(msg) ####
Adds a error to the error stack of the test

Parameter | Type         | Default | Info
----------| ------------ | ------- |----------------------
msg       | String       |         | Message of this error


## Tester ##
### Tester Fields ###

Field | Type | Info
---| --| --| --
queued_tests | List | All currently queued test objects
base_url | String | The root url the tester is using to execute all tests

### Tester Functions ###

- - - -

#### Tester.create_test(method, url, assertion, title, payload, files, queue) ####
Creates a new TestObject and adds it to the queued tests list

Parameter | Type         | Default        | Info
----------| ------------ | -------------- |--------------------------------------------------------
method    | String       |                | GET, POST, DELETE, PATCH or PUT
url       | String       |                | The route to execute this test for
assertion | Dict or List |                | The assertion about the server response
title     | String       | "Unnamed Test" | Title for this test
payload   | Dict         | None           | Additional values for the request
files     | Dict         | None           | A dict of open file objects to upload to the server
queue     | Boolean      | True           | If this test should be added to the current test queue

Returns **TestObject**

- - - -

#### Tester.evaluate_queued_tests(debug) ####
Executes all queued tests

Parameter | Type         | Default        | Info
----------| ------------ | -------------- |--------------------------------------------------------
debug     | Boolean      | False          | If true, prints process to the stdout

Returns **NUFFN**

- - - -

#### Tester.evaluate(test_object) ####
Executes a single tests for the specified test object

Parameter | Type         | Default        | Info
----------| ------------ | -------------- |--------------------------------------------------------
test_object     | TestObject      |           | The test object to execute a test for

Returns **NUFFN**

- - - -

#### Tester.summary(complete) ####
Prints a summary of all queued tests

Parameter | Type         | Default        | Info
----------| ------------ | -------------- |--------------------------------------------------------
complete     | Boolean      |           | If true, prints all details for the failed tests, if false only prints faield titles

Returns **NUFFN**

- - - -

#### Tester.print_failed() ####
Prints all failed tests

Returns **NUFFN**

- - - -

#### Tester.print_all() ####
Prints information about all tests

Returns **NUFFN**

- - - -

#### Tester.generate_html_output() ####
Generates HTML Code with all information about the queued tests.

Returns **String**