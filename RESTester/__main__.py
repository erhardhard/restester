"""Main Modul for restester"""
from __future__ import print_function

import argparse
from core import Tester
from utils import FileParser


if __name__ == '__main__':
    ARG_PARSER = argparse.ArgumentParser()
    ARG_PARSER.add_argument("TEST", type=file, help="The File with all tests")
    ARG_PARSER.add_argument("-o", "--output", dest="summary_file")

    ARGS = ARG_PARSER.parse_args()
    FILE_PARSER = FileParser()
    ALL_TESTS = FILE_PARSER.parse_file(ARGS.TEST)

    TESTER = Tester(ALL_TESTS['base_url'])
    for TEST in ALL_TESTS['tests']:
        files = {}
        if TEST['files']:
            for key in TEST['files']:
                try:
                    files[key] = open(TEST['files'][key], 'r')
                except IOError:
                    print("Failed to open file {0!s}".format(TEST['files'][key]))

        TESTER.create_test(
            TEST['method'],
            TEST['url'],
            TEST['assertion'],
            title=TEST['title'],
            payload=TEST['payload'],
            files=files)

    print("Process all queued tests...")
    TESTER.evaluate_queued_tests(debug=True)

    if ARGS.summary_file:
        HTML_OUPUT = TESTER.generate_html_output()
        with open(ARGS.summary_file, 'w+') as summary_file:
            summary_file.write(HTML_OUPUT)
    else:
        TESTER.summary()
