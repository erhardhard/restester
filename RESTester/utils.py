"""This Module defines several parsers for custom test file"""
from __future__ import print_function
from json import loads

VERSION = 1

class FileParser(object):
    """This Object can parse a custom test file"""

    def parse_file(self, test_file):
        """Parses a file"""
        tests = {}
        if int(test_file.readline().rstrip().split(" ")[1]) != VERSION:
            self.print_error(1, "Wrong VERSION")
            return {}

        tests['base_url'] = test_file.readline().rstrip()
        tests['tests'] = []

        test_mode = False
        current_test = {}
        content = []

        for line in test_file:
            line = line.strip()
            if line == "":
                continue
            elif line.startswith("TEST "):
                test_mode = True
                title = line[5:]
                current_test['title'] = title
                print("Parsing {0!s}".format(title))
            elif line == "END":
                test_mode = False
                current_test.update(loads("{" + "".join(content).replace(" ", "") + "}"))
                if 'files' not in current_test:
                    current_test['files'] = None
                if 'payload' not in current_test:
                    current_test['payload'] = None
                tests['tests'].append(current_test)
                print("{0!s} was OK".format(current_test['title']))
                current_test = {}
                content = []
            elif test_mode:
                content.append(line)
        return tests

    def print_error(self, line_no, error_text):
        """Print an error to the console"""
        print("ERROR at line {0!s}:\n{1!s}".format(line_no, error_text))
