"""This Module contains """

from __future__ import print_function
import re
from json import loads
import requests
from console_tools import printProgress

class Tester(object):
    """A Object to add all test to and to handle all testing"""
    queued_tests = []
    base_url = ""
    global_vars = {}

    def __init__(self, base_url):
        self.base_url = base_url

    def create_test(self, method, url, assertion,
                    title="Unnamed Test", payload=None, files=None, queue=True):
        """Creates a test based on all given testdata"""
        test_object = TestObject()
        test_object.title = title
        test_object.method = method
        test_object.url = self.base_url + url
        test_object.assertion = assertion
        test_object.payload = payload
        test_object.files = files
        if queue:
            self.queued_tests.append(test_object)
        return test_object

    def evaluate_queued_tests(self, debug=False):
        """Iterates over all queued tests and executes them"""
        for idx, test_object in enumerate(self.queued_tests):
            if debug:
                printProgress(idx, len(self.queued_tests), postfix=" of all tests executed")
            self.__pull_data(test_object)
            self.evaluate(test_object)

    def __pull_data(self, test_object):
        """Retrieves data from server"""
        if test_object.payload:
            for key in test_object.payload:
                if str(test_object.payload[key]).startswith('*'):
                    if test_object.payload[key] in self.global_vars:
                        test_object.payload[key] = self.global_vars[test_object.payload[key]]

        try:
            if test_object.method == 'GET':
                test_object.result_raw = requests.get(test_object.url)
            elif test_object.method == 'POST':
                test_object.result_raw = requests.post(
                    test_object.url,
                    data=test_object.payload,
                    files=test_object.files
                )
            elif test_object.method == 'PUT':
                test_object.result_raw = requests.put(
                    test_object.url,
                    data=test_object.payload,
                    files=test_object.files)
            elif test_object.method == 'PATCH':
                test_object.result_raw = requests.patch(
                    test_object.url,
                    data=test_object.payload,
                    files=test_object.files)
            elif test_object.method == 'DELETE':
                test_object.result_raw = requests.delete(test_object.url)
            else:
                test_object.push_error("unsupported method")
                test_object.passed = False
        except requests.exceptions.RequestException as conn_error:
            test_object.push_error("request failed with {0!s}".format(str(conn_error)))
            test_object.passed = False

    def evaluate(self, test_object):
        """Evaluates received data"""
        test_object.executed = True

        if not test_object.passed:
            return

        if test_object.result_raw == "":
            test_object.passed = False
            return

        test_object.result = loads(test_object.result_raw.text)
        test_object.status = test_object.result_raw.status_code
        if test_object.status != 200:
            test_object.passed = False
            return

        if isinstance(test_object.result, dict):
            test_object.passed = self.__test_dicts(test_object.result, test_object.assertion)
        elif isinstance(test_object.result, list):
            test_object.passed = self.__test_lists(test_object.result, test_object.assertion)
        else:
            test_object.push_error("non json return")

    def __test_lists(self, result, assertion):
        """Test if a list equals a assertion"""
        passed = True
        result = sorted(result)
        assertion = sorted(assertion)

        if len(result) != len(assertion):
            return False

        for elem in result:
            if passed:
                if isinstance(elem, list):
                    if isinstance(assertion, list):
                        passed = self.__test_lists(elem, assertion[0])
                    else:
                        passed = False
                elif isinstance(elem, dict):
                    if isinstance(assertion[0], dict):
                        passed = self.__test_dicts(elem, assertion[0])
                    else:
                        passed = False
                else:
                    mask = re.compile(str(assertion[0]) + '$')
                    match = mask.match(str(elem))
                    if not match:
                        passed = False

        return passed


    def __test_dicts(self, result, assertion):
        """Test if a dict equals a assertion"""
        passed = True
        for origk in assertion:
            k = origk
            if str(origk).startswith('*'):
                k = origk[1:]

            if passed:
                if not k in result:
                    passed = False
                else:
                    elem1 = result.get(k)
                    elem2 = assertion.get(origk)

                    if isinstance(elem1, dict):
                        if isinstance(elem2, dict):
                            passed = self.__test_dicts(elem1, elem2)
                        else:
                            passed = False
                    elif isinstance(elem1, list):
                        if isinstance(elem2, list):
                            passed = self.__test_lists(elem1, elem2)
                        else:
                            passed = False
                    else:
                        mask = re.compile(str(elem2) + '$')
                        match = mask.match(str(elem1))
                        if not match:
                            passed = False
                        else:
                            if str(origk).startswith('*'):
                                self.global_vars[origk] = str(elem1)

        return passed

    def summary(self, complete=False):
        """Generates a summary of all test objects"""
        print("### Test Summary ###")
        passed = [test_object for test_object in self.queued_tests if test_object.passed]
        failed_tests = [test_object for test_object in self.queued_tests if not test_object.passed]
        print("{0!s} of {1!s} tests succeeded".format(len(passed), len(self.queued_tests)))
        if complete:
            for test_object in failed_tests:
                print(test_object)
        else:
            for test_object in failed_tests:
                print("{0!s} failed".format(test_object.title))

    def print_failed(self):
        """Print all failed tests"""
        failed_tests = [test_object for test_object in self.queued_tests if not test_object.passed]
        for test_object in failed_tests:
            print(test_object)

    def print_all(self):
        """Prints all test objects"""
        for test_object in self.queued_tests:
            print(test_object)

    def generate_html_output(self):
        """Generates readable html with all summary informations"""
        head = """
<!doctype html>
<html lang="de">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>TestSummary</title>
</head>
<body>"""
        tail = """
</body>
</html>"""

        content = []
        for idx, test_object in enumerate(self.queued_tests):
            if test_object.passed:
                content.append(
                    '<font color="green">{0!s}</font><br>'.format(test_object.title))
            else:
                content.append(
                    '<font color="red">{0!s}</font><br>'.format(test_object.title))

            content.append('{0!s} {1!s}<br>'.format(test_object.method, test_object.url))
            content.append('<div id="spoiler{0!s}" style="display:none">'.format(idx))
            content.append('<pre style="background-color:#EBECE4; white-space: pre-wrap;">{0!s}</pre>'.format(test_object))
            content.append('</div>')
            content.append(("""<button title="Click to show/hide content" type="button" onclick="if(document.getElementById('spoiler""" + str(idx) + """').style.display=='none'){document.getElementById('spoiler""" + str(idx) + """').style.display=''}else{document.getElementById('spoiler""" + str(idx) + """').style.display='none'}">Show/hide</button><br>""").strip())
            content.append("<hr>")

        return head + "\n".join(content) + tail

class TestObject(object):
    """A simple Test Object to store all relevant data"""

    title = ""
    method = ""
    url = ""
    assertion = {}
    payload = {}
    files = {}
    result_raw = ""
    result = {}
    status = 0
    passed = True
    executed = False
    error_stack = []

    def push_error(self, msg):
        """Add a error to this test object"""
        self.error_stack.append(msg)

    def __str__(self):
        str_mask = """{0!s}
    {1!s} {2!s}
    Executed   : {3!s}
    Passed     : {4!s}
    Status     : {5!s}

    Payload    : {6!s}
    Files      :
{7!s}

    Assertion  : {8!s}
    Result     : {9!s}
                        
    ErrorStack :
    {10!s}"""
        return str_mask.format(
            self.title,
            self.method,
            self.url,
            self.executed,
            self.passed,
            self.status,
            self.payload,
            "\n".join(["    {0!s} : {1!s}".format(file_key, self.files[file_key].name) for file_key in self.files]) if self.files else "None",
            self.assertion,
            self.result,
            self.error_stack)
