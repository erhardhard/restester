"""Defines some usefull console tools"""

import sys

def printProgress(progress, max_progress, postfix=""):
    """Print progressbar"""
    percentual_progress = (float(progress) / float(max_progress) * 100)
    sys.stdout.write('\r')
    sys.stdout.write("[%-20s] %.2f%%" % ('='*int(percentual_progress/5) + '>', percentual_progress) + postfix)
    sys.stdout.flush()

def printRunningIndicator(id, prefix = "", postfix = ""):
    """Print running indicator"""
    val = id%4
    sys.stdout.write('\r')
    if val == 0 or val == 3:
        sys.stdout.write(prefix + "-" + postfix)
    elif val == 1:
        sys.stdout.write(prefix + "/" + postfix)
    else:
        sys.stdout.write(prefix + "\\" + postfix)
    sys.stdout.flush()
